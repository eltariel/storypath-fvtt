export class RollForm extends FormApplication {
    constructor(actor, options, object, data) {
        super(object, options);
        this.actor = actor;
        var attirubteFilter = "none";
        if(data.rollType !== 'base') {
            if(this.actor.data.type === 'npc') {
                if (data.pool) {
                    this.object.pool = data.pool;
                }
                else {
                    this.object.pool = "primary";
                }
            }
            else {
                if (data.skill === "defense") {
                    attirubteFilter = "resistance";
                }
                if (data.rollType === 'initiative' && this.actor.data.type !== "npc") {
                    this.object.attribute = 'cunning';
                }
                else {
                    if(data.attribute) {
                        this.object.attribute = data.attribute;
                    }
                    else {
                        this.object.attribute = this._getHighestAttribute(this.actor.data.data, attirubteFilter);
                    }
                }
                if(data.skill) {
                    if (data.skill === "defense") {
                        this.object.skill = "none";
                    }
                    else if(data.skill === "movement") {
                        this.object.skill = "athletics";
                        this.object.attribute = "might";
                    }
                    else {
                        this.object.skill = data.skill;
                    }
                }
                else {
                    this.object.skill = "none";
                }
            }
            this.object.characterType = this.actor.data.type;
        }
        else {
            this.object.dice = 0;
        }
        if(data.rollType) {
            this.object.rollType = data.rollType;
        }
        else {
            this.object.rollType = "skill";
        }
        this.object.itemDice = data.itemDice || 0;
        this.object.rollType = data.rollType;
        this.object.targetNumber = 8;
        this.object.diceModifier = 0;
        this.object.enhancement = 0;
        this.object.enhancementOnZero = false;
        this.object.difficulty = 0;

        this.object.doubleSuccess = 11;
        this.object.doubleSuccesses = false;
        this.object.numberAgain = 10;
        this._getSpecialties();
      }

      static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
          classes: ["dialog", `${game.settings.get("storypath-fvtt", "sheetStyle")}-background`],
          popOut: true,
          template: "systems/storypath-fvtt/templates/dialogues/skill-roll.html",
          id: "roll-form",
          title: `Roll`,
          width: 350,
          submitOnChange: true,
          closeOnSubmit: false
        });
      }

      getData() {
        return {
          actor: this.actor,
          data: this.object,
        };
      }

      async _updateObject(event, formData) {
        mergeObject(this, formData);
      }
      
      activateListeners(html) {
        super.activateListeners(html);

        html.find('#roll-button').click((event) => {
            this._roll();
            this.close();
        });

        html.find('#cancel').click((event) => {
            this.close();
        });

        html.find('.collapsable').click(ev => {
            const li = $(ev.currentTarget).next();
            li.toggle("fast");
        });
      }

    _baseSkillDieRoll() {
        let dice = 0;
        if (this.object.rollType === 'base') {
            dice = this.object.dice;
        }
        else {
            if (this.actor.data.type === 'scion') {
                let attributeDice = 0;
                if(this.object.attribute !== "none") {
                    attributeDice = this.actor.data.data.attributes[this.object.attribute].value;
                }
                let skillDice = 0;
                if (this.object.rollType == 'itemRoll') {
                    skillDice += this.object.itemDice;
                }

                if (this.object.skill !== "none") {
                    if (this.object.skill === "legend") {
                        skillDice += this.actor.data.data.legend.total;
                    }
                    else {
                        skillDice += this.actor.data.data.skills[this.object.skill].value;
                    }
                }
    
                dice = attributeDice + skillDice;
            }
            else if (this.actor.data.type === 'npc') {
                let poolDice = 0;
                if (this.object.pool === 'initiative') {
                    poolDice = this.actor.data.data[this.object.pool].value;
                }
                else {
                    poolDice = this.actor.data.data.pools[this.object.pool].value;
                }
                dice = poolDice;
            }
            if (this.actor.data.type === 'scion' && this.object.hasSpecialty) {
                this.object.enhancement++;
            }    
        }
    
        if (this.object.diceModifier) {
            dice += this.object.diceModifier;
        }
    
        let roll = new Roll(`${dice}d10x>=${this.object.numberAgain}cs>=${this.object.targetNumber}`).evaluate({ async: false });
        let diceRoll = roll.dice[0].results;
        let getDice = "";
    
        let bonus = 0;
    
        for (let dice of diceRoll) {
            if (dice.result >= this.object.doubleSuccess) {
                bonus++;
                getDice += `<li class="roll die d10 success double-success">${dice.result}</li>`;
            }
            else if (dice.result >= this.object.targetNumber) { getDice += `<li class="roll die d10 success">${dice.result}</li>`; }
            else if (dice.result == 1) { getDice += `<li class="roll die d10 failure">${dice.result}</li>`; }
            else { getDice += `<li class="roll die d10">${dice.result}</li>`; }
        }
    
        let total = roll.total;
        if (this.object.doubleSuccesses) total = total * 2;
        if (bonus) total += bonus;
        if (this.object.enhancement && (total > 0 || this.object.enhancementOnZero)) total += this.object.enhancement;

        this.object.dice = dice;
        this.object.roll = roll;
        this.object.getDice = getDice;
        this.object.total = total;
    }

    _roll() {
        this._baseSkillDieRoll();
        let resultString = ``;
        if (this.object.difficulty) {
            if (this.object.total < this.object.difficulty) {
                resultString = `<h4 class="dice-total">Difficulty: ${this.object.difficulty}</h4><h4 class="dice-total">Check Failed</h4>`;
                for (let dice of this.object.roll.dice[0].results) {
                    if (dice.result === 1 && this.object.total === 0) {
                        resultString = `<h4 class="dice-total">Difficulty: ${this.object.difficulty}</h4><h4 class="dice-total">Botch</h4>`;
                    }
                }
            }
            else {
                const threshholdSuccesses = this.object.total - this.object.difficulty;
                let successString = 'Normal';
                if (threshholdSuccesses === 1) {
                    successString = 'Competent';
                }
                else if (threshholdSuccesses === 2) {
                    successString = 'Excellent';
                }
                else if (threshholdSuccesses === 3) {
                    successString = 'Amazing';
                }
                else if (threshholdSuccesses >= 4) {
                    successString = 'Divine';
                }
                resultString = `<h4 class="dice-total">Difficulty: ${this.object.difficulty}</h4><h4 class="dice-total">${threshholdSuccesses} Threshhold ${threshholdSuccesses === 1 ? 'Success' : 'Successes'}</h4><h4 class="dice-total">${successString} Success</h4>`;
            }
        }
        let the_content = `
        <div class="chat-card ${game.settings.get("storypath-fvtt", "sheetStyle")}-dice-background">
            <div class="card-content">Dice Roll</div>
            <div class="card-buttons">
                <div class="flexrow 1">
                    <div>Dice Roller - Number of Successes<div class="dice-roll">
                            <div class="dice-result">
                                <h4 class="dice-formula">${this.object.dice} Dice + ${this.object.enhancement} Enhancement</h4>
                                <div class="dice-tooltip">
                                    <div class="dice">
                                        <ol class="dice-rolls">${this.object.getDice}</ol>
                                    </div>
                                </div>
                                <h4 class="dice-total">${this.object.total} Successes</h4>
                                ${resultString}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `
        ChatMessage.create({ user: game.user.id, speaker: ChatMessage.getSpeaker({ actor: this.actor }), content: the_content, type: CONST.CHAT_MESSAGE_TYPES.ROLL, roll: this.object.roll });
        if (this.object.rollType === "initiative" || this.object.skill === 'initiative') {
            let combat = game.combat;
            if (combat) {
                let combatant = combat.data.combatants.find(c => c?.actor?.data?._id == this.actor.id);
                if (combatant) {
                    combat.setInitiative(combatant.id, this.object.total);
                }
            }
        }
    }

    _getSpecialties() {
        var specialties = [];
        for (let [name, specialty] of Object.entries(this.actor.specialties)) {
            // if(specialty.data.skill === this.object.skill) {
                specialties.push(specialty);
            // }
        }
        this.object.specialties = specialties;
    }


    _getHighestAttribute(data, approachFilter) {
        var highestAttributeNumber = 0;
        var highestAttribute;
        for (let [name, attribute] of Object.entries(data.attributes)) {
            if (approachFilter === attribute.approach || approachFilter === "none") {
                if (attribute.value > highestAttributeNumber || highestAttributeNumber === 0) {
                    highestAttributeNumber = attribute.value;
                    highestAttribute = name;
                }
            }
        }
        return highestAttribute;
    }
}