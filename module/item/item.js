/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class StorypathItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    // Get the Item's data
    const itemData = this.data;
    const actorData = this.actor ? this.actor.data : {};
    const data = itemData.data;
  }

  async _preCreate(createData, options, userId) {
    if (!createData.img) {
      this.data.update({ img: getDefaultImage(createData.type) });
    }
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async roll() {
    // Basic template rendering data
    const token = this.actor.token;
    const item = this.data;
    const actorData = this.actor ? this.actor.data.data : {};
    const itemData = item.data;
  }
}

function getDefaultImage(type) {
  const defaultImages = {
    'knack': "icons/svg/light.svg",
    'contact': "icons/svg/mystery-man.svg",
    'path': "icons/svg/thrust.svg",
    'specialty': "icons/svg/upgrade.svg",
    'quality': "icons/svg/aura.svg",
    'flair': "icons/svg/fire.svg",
    'birthright': "icons/svg/chest.svg",
    'boon': "icons/svg/fire.svg",
    'purview': "icons/svg/fire.svg",
    'condition': "icons/svg/daze.svg",
    'calling': "icons/svg/clockwork.svg",
    'health': "icons/svg/regen.svg",
    'fatebinding': "icons/svg/explosion.svg"
  };
  return defaultImages[type] || CONST.DEFAULT_TOKEN;
}
